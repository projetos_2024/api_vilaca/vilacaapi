const router = require('express').Router()
const teacherController = require('../controller/teacherController');
const JSONPlaceholderController = require('../controller/JSONPlaceholderController');

router.get('/teacher/', teacherController.getTeacher);
router.post('/cadastroAluno/', teacherController.postAluno);
router.put('/cadastroAluno/', teacherController.updateAluno);
router.delete('/cadastroAluno/:id', teacherController.deleteAluno);

router.get("/external/", JSONPlaceholderController.getUsers);
router.get("/external/io", JSONPlaceholderController.getUsersWebSiteIO);
router.get("/external/com", JSONPlaceholderController.getUsersWebSiteCOM);
router.get("/external/net", JSONPlaceholderController.getUsersWebSiteNET);
router.get('/external/filter', JSONPlaceholderController.getCountDomain);

module.exports = router
